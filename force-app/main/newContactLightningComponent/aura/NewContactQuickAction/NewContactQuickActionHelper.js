({
    createRecord: function(component) {
        const createRecordEvent = $A.get('e.force:createRecord');
        const account = component.get('v.account');

        createRecordEvent.setParams({
            entityApiName: 'Contact',
            recordTypeId: component.get('v.recordTypeId'),
            defaultFieldValues: {
                // NOTE: add field mappings here, but make sure you update the
                // list of fields set on the <force:recordData> component.
                // Also make sure the fields appear on the page layouts
                // otherwise you get an internal server error.
                AccountId: account.Id,
                MailingStreet: account.BillingStreet,
                MailingCity: account.BillingCity,
                MailingState: account.BillingState,
                MailingPostalCode: account.BillingPostalCode,
                MailingCountry: account.BillingCountry
            }
        });

        createRecordEvent.fire();
    },

    getRecordTypeOptions: function(component) {
        this.enqueueAction(component, 'c.getRecordTypeOptions')
            .then(function(response) {
                const recordTypeOptions = response.getReturnValue();

                component.set('v.recordTypeOptions', recordTypeOptions);

                recordTypeOptions.forEach(function(option) {
                    if (option.isDefault)
                    {
                        component.set('v.recordTypeId', option.recordTypeId);
                    }
                });
            })
            .catch(function(response) {
                this.handleResponse(component, response);
            });
    },

    enqueueAction: function(component, action, params) {
        return new Promise($A.getCallback(function(resolve, reject) {
            const a = component.get(action);

            if (params) {
                a.setParams(params);
            }

            a.setCallback(this, function(response) {
                if (response.getState() === 'SUCCESS') {
                    resolve(response);
                }
                else {
                    reject(response);
                }
            });

            $A.enqueueAction(a);
        }));
    },

    handleResponse: function(component, response) {
        const state = response.getState();

        if (state === 'INCOMPLETE') {
            this.showToast({
                title: 'Offline',
                message: 'You are currently offline.',
                type: 'info'
            });
        }
        else if (state === 'ERROR') {
            const errors = response.getError();

            if (errors) {
                if (errors[0] && errors[0].message) {
                    this.showToast({
                        title: 'Error',
                        message: errors[0].message,
                        type: 'error'
                    });
                }
            }
            else {
                this.showToast({
                    title: 'Error',
                    message: 'An unkown error occurred.',
                    type: 'error'
                });
            }
        }
    },

    handleRecordUpdate: function(component, event) {
        const params = event.getParams();

        if (params.changeType === 'ERROR') {
            this.showToast({
                title: 'Error',
                message: component.get('v.error'),
                type: 'error'
            });
        }
    },

    showToast: function(params) {
        const toast = $A.get('e.force:showToast');
        toast.setParams(params);
        toast.fire();
    },

    closeQuickAction: function() {
        const closeQuickAction = $A.get('e.force:closeQuickAction');
        closeQuickAction.fire();
    }
})
