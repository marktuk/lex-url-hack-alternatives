({
    onInit: function(component, event, helper) {
        helper.getRecordTypeOptions(component);
    },

    onRecordUpdate: function(component, event, helper) {
        helper.handleRecordUpdate(component, event);
    },

    onNext: function(component, event, helper) {
        helper.createRecord(component);
    },

    onCancel: function(component, event, helper) {
        helper.closeQuickAction();
    }
})