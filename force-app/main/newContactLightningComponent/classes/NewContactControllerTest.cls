@isTest
private class NewContactControllerTest {

    @isTest
    private static void testGetRecordTypeOptions() {
        Map<Id, Schema.RecordTypeInfo> recordTypeInfosById = Contact.SObjectType.getDescribe().getRecordTypeInfosById();

        Test.startTest();
            List<NewContactController.RecordTypeOption> recordTypeOptions = NewContactController.getRecordTypeOptions();
        Test.stopTest();

        for (NewContactController.RecordTypeOption option : recordTypeOptions) {
            Schema.RecordTypeInfo recordType = recordTypeInfosById.get(option.recordTypeId);

            System.assertNotEquals(null, recordType);
            System.assert(recordType.isActive());
            System.assert(recordType.isAvailable());
            System.assertEquals(option.recordTypeId, recordType.getRecordTypeId());
            System.assertEquals(option.recordTypeName, recordType.getName());
            System.assertEquals(option.isDefault, recordType.isDefaultRecordTypeMapping());
        }
    }

}
