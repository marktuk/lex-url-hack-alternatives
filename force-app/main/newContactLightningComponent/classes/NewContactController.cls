public with sharing class NewContactController {

    @AuraEnabled(cacheable=true)
    public static List<RecordTypeOption> getRecordTypeOptions() {
        List<RecordTypeOption> recordTypeOptions = new List<RecordTypeOption>();
        List<Schema.RecordTypeInfo> recordTypeInfos = Contact.SObjectType.getDescribe().getRecordTypeInfos();

        for (Schema.RecordTypeInfo recordType : recordTypeInfos) {
            if (recordType.isActive() && isRecordTypeAvailable(recordType)) {
                recordTypeOptions.add(new RecordTypeOption(
                    recordType.getRecordTypeId(),
                    recordType.getName(),
                    recordType.isDefaultRecordTypeMapping()
                ));
            }
        }

        return recordTypeOptions;
    }

    // NOTE: this check is needed because the master record type will always
    // return as available. Unless it's set as the profile default, it's assumed
    // it's not available.
    private static Boolean isRecordTypeAvailable(RecordTypeInfo recordType) {
        if (recordType.isMaster() && !recordType.isDefaultRecordTypeMapping()) {
            return false;
        }
        else {
            return recordType.isAvailable();
        }
    }

    public class RecordTypeOption {
        @AuraEnabled public Id recordTypeId;
        @AuraEnabled public String recordTypeName;
        @AuraEnabled public Boolean isDefault;

        public RecordTypeOption(Id recordTypeId, String recordTypeName, Boolean isDefault) {
            this.recordTypeId = recordTypeId;
            this.recordTypeName = recordTypeName;
            this.isDefault = isDefault;
        }
    }

}
