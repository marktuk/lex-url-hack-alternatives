({
    navigateToRecord: function(component) {
        const navigateToSObject = $A.get('e.force:navigateToSObject');

        navigateToSObject.setParams({
            recordId: component.get('v.recordId'),
            slideDevName: component.get('v.slideDevName')
        });

        navigateToSObject.fire();
    }
})
