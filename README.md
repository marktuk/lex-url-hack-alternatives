# Alternatives to URL "hack" custom buttons for Lightning Experience

A collection of Lightning Experience ready alternatives to URL "hack" custom
buttons.

Read the [blog post](https://marktyrrell.com/posts/alternatives-to-custom-button-url-hacks-for-lightning-experience/)
to learn more about each solution.

## Installation
Install the project in a scratch org or developer org using the Salesforce CLI.

### Developer Org
```bash
sfdx force:source:deploy -u <username> -p force-app
```

### Scratch Org
```bash
sfdx force:source:push -u <username>
```

## Issues
If you find any bugs in the code or the blog post don't be afraid to raise a
[new issue](https://gitlab.com/marktuk/lex-url-hack-alternatives/issues/new).

## License
Released under the terms of the [MIT License](LICENSE.md).